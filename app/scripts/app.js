'use strict';

/**
 * @ngdoc overview
 * @name angularApp
 * @description
 * # angularApp
 *
 * Main module of the application.
 */
angular
  .module('angularApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'restangular'
  ])
  .run(function ($rootScope, $http, Restangular) {
    $rootScope.debug = false;
    $rootScope.isDebug = function () {
      return $rootScope.debug;
    };
    $rootScope.toggleDebug = function () {
      if ($rootScope.debug) {
        $rootScope.debug = false;
      } else {
        $rootScope.debug = true;
      }
    }

  })
  .filter('checkHaircut', function () {
    return function (input) {
      if (input) {
        console.log(input);
        var now = Date.now();
        var next = new Date(input).getTime();
        var time = now >= next;
        console.log(now);
        console.log(next);
        return time ? 'можно стричь' : 'через ' + Math.round(next / (1000 * 60));
      }
    }
  })
  .config(function ($routeProvider) {
    $routeProvider
      /*.when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })*/
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/sheep', {
        templateUrl: 'views/sheep.html',
        controller: 'SheepCtrl'
      })
      .when('/profile', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      })
      .when('/user', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl'
      })
      .when('/user/:id', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      })
      .otherwise({
        redirectTo: '/profile'
      });

  })
  .config(function (RestangularProvider, $httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    RestangularProvider.setDefaultHeaders({
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
    });
    var local = 'http://localhost:1337/';
    var main = 'https://dry-chamber-2260.herokuapp.com/';
    RestangularProvider.setBaseUrl(local);
  });
