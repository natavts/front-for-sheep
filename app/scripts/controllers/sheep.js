'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:SheepCtrl
 * @description
 * # SheepCtrl
 * Controller of the angularApp
 */
angular.module('angularApp')
  .controller('SheepCtrl', function ($scope, Restangular, server) {
    $scope.sheeps = server.sheep.allSheepObj;
    $scope.server = server;
  });
