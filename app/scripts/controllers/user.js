'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the angularApp
 */
angular.module('angularApp')
  .controller('UserCtrl', function ($rootScope, $scope, Restangular, server, $location, $routeParams) {
    console.log($routeParams.id)
    server.user.userPromise().then(function (data) {
      $scope.user = {
        id: $routeParams.id || data.id,
        login: function () {
          var self = this;
          Restangular.one('auth', 'external').one('{"username": "54d1ed7b660967c67bcf0309"}').get();
        }
      };
      server.user.one($scope.user.id).then(function (data) {
        $scope.user.data = data;
        $scope.sheeps = [];
        data.sheep.forEach(function (sheep) {

          Restangular.one('sheep', sheep.id).get().then(function (data) {
            $scope.sheeps.push(data);
          })
        });
      });

    });

    $scope.server = server;


  });
