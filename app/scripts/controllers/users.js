'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the angularApp
 */
angular.module('angularApp')
  .controller('UsersCtrl', function ($scope, server) {
    $scope.users = server.user.allUsers().$object;
  });
