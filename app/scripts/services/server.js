'use strict';

/**
 * @ngdoc service
 * @name angularApp.server
 * @description
 * # server
 * Service in the angularApp.
 */
angular.module('angularApp')
  .service('server', function ($rootScope, Restangular) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.sheep = {
      allSheepObj: Restangular.all('sheep').getList().$object,
      all: function () {
        Restangular.all('sheep').getList()
      },
      one: function (id) {
        return Restangular.one('sheep', id)
      },
      cut: function (id) {
        console.log(this);
        this.one(id).one('cut').get().then(function (data) {
          alert(data.response);
          user.updateMyUserObj();
        })
      }
    };
    this.user = {
      userPromise: function(){
        return Restangular.one('user', 'my').get()
      },
      one: function (id) {
        return Restangular.one('user', id).get()
      },
      allUsers: function(){
        return Restangular.one('user').getList()
      },
      myUserObj: Restangular.one(this.myId).get().$object,
      updateMyUserObj: function () {
        var self = this;
        console.log(this);
        this.myUserObj = this.one(self.myId).$object;
      }
    };
    this.action = {
      cut: function (id) {
        sheep.cut(id);
      }
    };
    var user = this.user
    var sheep = this.sheep

  });
